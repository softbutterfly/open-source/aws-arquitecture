# Laboratorio de Instalación de Agente de New Relic Infrastructure en Kubernetes para Azure Cache For Redis y Azure SQL Database

## Arquitectura de Laboratorio

![arquitectura](demo-notebook/_assets/arquitectura.png)

## Instalación de Agente de New Relic de Infraestructura en Kubernetes

Ejecución de Script de Instalación:-

![arquitectura](demo-notebook/_assets/ejecucion_script.png)

Luego de ejecutar el script correctamente, se obtiene el siguiente mensaje:

![arquitectura](demo-notebook/_assets/respuesta_ejecucion_script.png)

Validación de Recepción de datos en la Plataforma de New Relic:

![arquitectura](demo-notebook/_assets/validacion_datos_nr.png)

![arquitectura](demo-notebook/_assets/validacion_datos_nr2.png)

## Preparación y Configuración de SQL Database

Creación de nuevo login para monitoreo y asignación de roles:

![arquitectura](demo-notebook/_assets/creacion_usuario_asignacion_roles.png)

Selección de base de datos a monitorear

![arquitectura](demo-notebook/_assets/bd_monitorear.png)

Creación de nuevo usuario en la base de datos a monitorear y asignación de login.

![arquitectura](demo-notebook/_assets/respuesta_creacion_usuario.png)

### En el cluster de Kubernetes:

Verificación los namespaces y los pods disponibles

![arquitectura](demo-notebook/_assets/verificacion_namespace_pods.png)

Creación del archivo values.yaml donde se ubicará la configuración de New Relic para el monitoreo de la base de datos.

![arquitectura](demo-notebook/_assets/creacion_archivo_yaml.png)

Ejecución de comandos para montar la configuración en el cluster de kubernetes:

![arquitectura](demo-notebook/_assets/comandos_configuracion_k8.png)

Validamos la configuración en el siguiente configmap: 
kubectl edit configmap newrelic-bundle-nrk8s-integrations-cfg  -n newrelic

![arquitectura](demo-notebook/_assets/validar_configuracion_configmap.png)

Validación de creación del nuevo pod:

![arquitectura](demo-notebook/_assets/creacion_pod.png)

Visualización de logs del nuevo pod:

![arquitectura](demo-notebook/_assets/visualizacion_logs_pod.png)

Validación de recepción de datos en la plataforma de New Relic:

![arquitectura](demo-notebook/_assets/validacion_datos_nr3.png)

Utilizamos la query show event types para mostrar las tablas disponibles, y observamos la recolección de datos en las tablas correspondientes a la integración instalada:

![arquitectura](demo-notebook/_assets/mostrar_tablas_disponibles.png)

## Configuración para monitoreo de  Azure Cache For Redis

Creación del archivo values-redis.yaml donde se ubicará la configuración de new relic para el monitoreo de la base de datos.

![arquitectura](demo-notebook/_assets/creacion_values_redis_yaml.png)

Ejecución de código para montar la configuración en el cluster de kubernetes:

![arquitectura](demo-notebook/_assets/ejecucion_codigo_k8.png)

Validamos la configuración en el siguiente configmap: 
kubectl edit configmap newrelic-bundle-nrk8s-integrations-cfg  -n newrelic

![arquitectura](demo-notebook/_assets/validar_conf_map.png)

Validación de creación del nuevo pod:

![arquitectura](demo-notebook/_assets/validacion_pod.png)

Visualización de log del nuevo pod:

![arquitectura](demo-notebook/_assets/visualizacion_log2.png)

Validación de recepción de datos en la plataforma de New Relic

![arquitectura](demo-notebook/_assets/validacion_datos_nr4.png)

Utilizamos la query show event types para mostrar las tablas disponibles, y observamos la recolección de datos en las tablas correspondientes a la integración instalada:

![arquitectura](demo-notebook/_assets/mostrar_tablas_disponibles2.png)

## Configuración para Monitoreo de Ambos Servicios desde el mismo Cluster

Creación del archivo values-final.yaml donde se ubicará la configuración de new relic para el monitoreo de ambos servicios.

![arquitectura](demo-notebook/_assets/creacion_values_final_yaml.png)

Validamos la configuración en el siguiente configmap: 
kubectl edit configmap newrelic-bundle-nrk8s-integrations-cfg  -n newrelic

![arquitectura](demo-notebook/_assets/validar_conf_map2.png)

En la plataforma podemos observar que ambas integraciones se muestran en la lista, y cada una muestra datos recientes:

![arquitectura](demo-notebook/_assets/integraciones_nr.png)

Redis:

![arquitectura](demo-notebook/_assets/integracion_redis.png)

MSSQL:

![arquitectura](demo-notebook/_assets/integracion_mysql.png)

Utilizamos la query show event types para mostrar las tablas disponibles, y observamos la recolección de datos en las tablas correspondientes a las integraciones instaladas:

![arquitectura](demo-notebook/_assets/mostrar_tablas_disponibles3.png)